﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNetCore.Mvc;
using System.Text.RegularExpressions;
using Elight.WebUI.Core.Controllers;
using Elight.WebUI.Core.Filters;
using Elight.Logic.Sys;
using Elight.Entity.Sys;
using Elight.Utility.ResponseModels;
using Elight.Utility.Extension;
using Elight.Utility.Core;

namespace Elight.WebUI.Core.Controllers
{
    [HiddenApi]
    public class PermissionController : BaseController
    {
        private SysPermissionLogic permissionLogic;

        public PermissionController()
        {
            permissionLogic = new SysPermissionLogic();
        }


        [Route("System/Permission/Index")]
        [HttpGet, AuthorizeChecked, LoginChecked]
        public ActionResult Index()
        {
            return View();
        }


        [Route("System/Permission/Index")]
        [HttpPost, AuthorizeChecked, LoginChecked]
        public ActionResult Index(int pageIndex, int pageSize, string keyWord)
        {
            int totalCount = 0;
            var pageData = permissionLogic.GetList(pageIndex, pageSize, keyWord, ref totalCount);
            var result = new LayPadding<SysPermission>()
            {
                result = true,
                msg = "success",
                list = pageData,
                count = totalCount//pageData.Count,
            };
            return Content(result.ToJson());
        }


        [Route("System/Permission/Form")]
        [HttpGet, AuthorizeChecked, LoginChecked]
        public ActionResult Form()
        {
            return View();
        }


        [Route("System/Permission/Form")]
        [HttpPost, AuthorizeChecked, LoginChecked]
        public ActionResult Form(SysPermission model)
        { 
            if (model.Id.IsNullOrEmpty())
            {
                int row = permissionLogic.Insert(model);
                return row > 0 ? Success() : Error();
            }
            else
            {
                int row = permissionLogic.Update(model);
                return row > 0 ? Success() : Error();
            }
        }


        [Route("System/Permission/Detail")]
        [HttpGet, AuthorizeChecked, LoginChecked]
        public ActionResult Detail()
        {
            return View();
        }


        [Route("System/Permission/Delete")]
        [HttpPost, AuthorizeChecked, LoginChecked]
        public ActionResult Delete(string primaryKey)
        {
            long count = permissionLogic.GetChildCount(primaryKey);
            if (count == 0)
            {
                int row = permissionLogic.Delete(primaryKey.SplitToList().ToArray());
                return row > 0 ? Success() : Error();
            }
            return Error(string.Format("操作失败，请先删除该项的{0}个子级权限。", count));
        }


        [Route("System/Permission/GetForm")]
        [HttpPost, LoginChecked]
        public ActionResult GetForm(string primaryKey)
        {
            SysPermission entity = permissionLogic.Get(primaryKey);
            entity.IsEdit = entity.IsEdit == "1" ? "true" : "false";
            entity.IsEnable = entity.IsEnable == "1" ? "true" : "false";
            entity.IsPublic = entity.IsPublic == "1" ? "true" : "false";
            return Content(entity.ToJson());
        }

        [Route("System/Permission/GetParent")]
        [HttpPost, LoginChecked]
        public ActionResult GetParent()
        {
            var data = permissionLogic.GetList();
            var treeList = new List<TreeSelect>();
            foreach (SysPermission item in data)
            {
                TreeSelect model = new TreeSelect();
                model.id = item.Id;
                model.text = item.Name;
                model.parentId = item.ParentId;
                treeList.Add(model);
            }
            return Content(treeList.ToTreeSelectJson());
        }

        [Route("System/Permission/Icon")]

        [HttpGet, LoginChecked]
        public ActionResult Icon()
        {
            return View();
        }



    }

}

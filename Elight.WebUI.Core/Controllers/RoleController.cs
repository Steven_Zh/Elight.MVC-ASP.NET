﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNetCore.Mvc;
using Elight.WebUI.Core.Controllers;
using Elight.WebUI.Core.Filters;
using Elight.Logic.Sys;
using Elight.Entity.Sys;
using Elight.Utility.ResponseModels;
using Elight.Utility.Extension;
using Elight.Utility.Core;

namespace Elight.WebUI.Core.Controllers
{
    [HiddenApi]
    public class RoleController : BaseController
    {
        private SysRoleLogic roleLogic;

        public RoleController()
        {
            roleLogic = new SysRoleLogic();
        }

        [Route("System/Role/Index")]
        [HttpGet, AuthorizeChecked, LoginChecked]
        public ActionResult Index()
        {
            return View();
        }

        [Route("System/Role/Index")]
        [HttpPost, AuthorizeChecked, LoginChecked]
        public ActionResult Index(int pageIndex, int pageSize, string keyWord)
        {
            int totalCount = 0;
            var pageData = roleLogic.GetList(pageIndex, pageSize, keyWord, ref totalCount);
            var result = new LayPadding<SysRole>()
            {
                result = true,
                msg = "success",
                list = pageData,
                count = totalCount// pageData.Count
            };
            return Content(result.ToJson());
        }


        [Route("System/Role/Form")]
        [HttpGet, AuthorizeChecked, LoginChecked]
        public ActionResult Form()
        {
            return View();
        }

        [Route("System/Role/Form")]
        [HttpPost, AuthorizeChecked,  LoginChecked]
        public ActionResult Form(SysRole model)
        { 
            if (model.Id.IsNullOrEmpty())
            {
                int row = roleLogic.Insert(model);
                return row > 0 ? Success() : Error();
            }
            else
            {
                int row = roleLogic.Update(model);
                return row > 0 ? Success() : Error();
            }
        }


        [Route("System/Role/Detail")]
        [HttpGet, AuthorizeChecked, LoginChecked]
        public ActionResult Detail()
        {
            return View();
        }


        [Route("System/Role/GetForm")]
        [HttpPost, LoginChecked]
        public ActionResult GetForm(string primaryKey)
        {
            SysRole entity = roleLogic.Get(primaryKey);
            entity.IsEnabled = entity.IsEnabled == "1" ? "true" : "false";
            entity.AllowEdit = entity.AllowEdit == "1" ? "true" : "false";
            return Content(entity.ToJson());
        }

        [Route("System/Role/Delete")]
        [HttpPost, AuthorizeChecked, LoginChecked]
        public ActionResult Delete(string primaryKey)
        {
            int row = roleLogic.Delete(primaryKey.SplitToList().ToArray());
            return row > 0 ? Success() : Error();
        }

        [Route("System/Role/GetListTreeSelect")]
        [HttpPost, LoginChecked]
        public ActionResult GetListTreeSelect()
        {
            List<SysRole> listRole = roleLogic.GetList();
            var listTree = new List<TreeSelect>();
            foreach (var item in listRole)
            {
                TreeSelect model = new TreeSelect();
                model.id = item.Id;
                model.text = item.Name;
                listTree.Add(model);
            }
            return Content(listTree.ToJson());
        }


    }


}

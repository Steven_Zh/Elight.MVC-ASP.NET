﻿using System.Web.Mvc;

namespace Elight.WebUI.Controllers
{
    public class IndexController : BaseController
    {
        /// <summary>
        /// 登陆页面视图。
        /// </summary>
        /// <returns></returns>
        [Route("ueditor.html")]
        [HttpGet]
        public ActionResult UEditor()
        {
            return View();
        }
    }
}
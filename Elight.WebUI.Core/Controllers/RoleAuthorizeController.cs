﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNetCore.Mvc;
using Elight.WebUI.Core.Controllers;
using Elight.WebUI.Core.Filters;
using Elight.Logic.Sys;
using Elight.Utility.ResponseModels;
using Elight.Utility.Core;

namespace Elight.WebUI.Core.Controllers
{
    [HiddenApi]
    public class RoleAuthorizeController : BaseController
    {
        private SysRoleAuthorizeLogic roleAuthorizeLogic;
        private SysPermissionLogic permissionLogic;

        public RoleAuthorizeController()
        {
            roleAuthorizeLogic = new SysRoleAuthorizeLogic();
            permissionLogic = new SysPermissionLogic();
        }


        [Route("System/RoleAuthorize/Index")]
        [HttpGet, LoginChecked]
        public ActionResult Index()
        {
            return View();
        }


        [Route("System/RoleAuthorize/Index")]
        [HttpPost, LoginChecked]
        public ActionResult Index(string roleId)
        {
            var listPerIds = roleAuthorizeLogic.GetList(roleId).Select(c => c.ModuleId).ToList();
            var listAllPers = permissionLogic.GetList();
            List<ZTreeNode> result = new List<ZTreeNode>();
            foreach (var item in listAllPers)
            {
                ZTreeNode model = new ZTreeNode();
                model.@checked = listPerIds.Contains(item.Id) ? model.@checked = true : model.@checked = false;
                model.id = item.Id;
                model.pId = item.ParentId;
                model.name = item.Name;
                model.open = true;
                result.Add(model);
            }
            return Content(result.ToJson());
        }


        [Route("System/RoleAuthorize/Form")]
        [HttpPost, LoginChecked]
        public ActionResult Form(string roleId, string perIds)
        {
            roleAuthorizeLogic.Authorize(roleId, perIds.SplitToList().ToArray());
            return Success("授权成功");
        }

        

    }
}

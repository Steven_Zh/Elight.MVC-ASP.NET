﻿using Microsoft.AspNetCore.Mvc;
using Elight.Entity.Sys;
using Elight.Logic.Sys;
using Elight.Utility.Core;
using Elight.Utility.ResponseModels;
using Elight.WebUI.Core.Filters;

namespace Elight.WebUI.Core.Controllers
{
    [HiddenApi]
    public class UserLogOnController : BaseController
    {
        private SysUserLogOnLogic userLogOnLogic;

        public UserLogOnController()
        {
            userLogOnLogic = new SysUserLogOnLogic();
        }


        [Route("System/UserLogOn/Form")]
        [HttpPost]
        public ActionResult Form([FromForm] SysUserLogOn model)
        {
            if (model.Id.IsNullOrEmpty())
            {
                int row = userLogOnLogic.Insert(model);
                return row > 0 ? Success() : Error();
            }
            else
            {
                var row = userLogOnLogic.UpdateInfo(model);
                return row > 0 ? Success() : Error();
            }
        }

    }
}

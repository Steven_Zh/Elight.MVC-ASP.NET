﻿using Elight.WebUI.Core.Controllers;
using Elight.WebUI.Core.Filters;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Elight.WebUI.Core.Controllers
{
    [HiddenApi]
    public class IndexController : BaseController
    {
        [Route("ueditor.html")]
        [HttpGet]
        public ActionResult UEditor()
        {
            return View();
        }
        [Route("Index/Error")]
        [HttpGet]
        public ActionResult Error()
        {
            return View();
        }

    }
}

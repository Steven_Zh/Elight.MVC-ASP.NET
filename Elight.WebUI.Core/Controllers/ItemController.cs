﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Elight.WebUI.Core.Controllers;
using Elight.WebUI.Core.Filters;
using Elight.Logic.Sys;
using Elight.Entity.Sys;
using Elight.Utility.ResponseModels;
using Elight.Utility.Extension;
using Microsoft.AspNetCore.Mvc;
using Elight.Utility.Core;

namespace Elight.WebUI.Core.Controllers
{
    [HiddenApi]
    public class ItemController : BaseController
    {
        private SysItemLogic itemLogic;
        private SysItemsDetailLogic itemsDetailLogic;

        public ItemController()
        {
            itemLogic = new SysItemLogic();
            itemsDetailLogic = new SysItemsDetailLogic();
        }


        [Route("System/Item/Index")]
        [HttpGet, AuthorizeChecked, LoginChecked]
        public ActionResult Index()
        {
            return View();
        }

        [Route("System/Item/Index")]
        [HttpPost, AuthorizeChecked, LoginChecked]
        public ActionResult Index(int pageIndex, int pageSize, string keyWord)
        {
            int totalCount = 0;
            var pageData = itemLogic.GetList(pageIndex, pageSize, keyWord, ref totalCount);
            var result = new LayPadding<SysItem>()
            {
                result = true,
                msg = "success",
                list = pageData,
                count = totalCount//pageData.Count
            };
            return Content(result.ToJson());
        }


        [Route("System/Item/Form")]
        [HttpGet, LoginChecked]
        public ActionResult Form()
        {
            return View();
        }


        [Route("System/Item/Form")]
        [HttpPost,  LoginChecked]
        public ActionResult Form(SysItem model)
        {
            if (model.Id.IsNullOrEmpty())
            {
                int row = itemLogic.Insert(model);
                return row > 0 ? Success() : Error();
            }
            else
            {
                int row = itemLogic.Update(model);
                return row > 0 ? Success() : Error();
            }
        }


        [Route("System/Item/GetForm")]
        [HttpPost, LoginChecked]
        public ActionResult GetForm(string primaryKey)
        {
            SysItem entity = itemLogic.Get(primaryKey);
            entity.IsEnabled = entity.IsEnabled == "1" ? "true" : "false";
            return Content(entity.ToJson());
        }

        [Route("System/Item/Delete")]
        [HttpPost, LoginChecked]
        public ActionResult Delete(string primaryKey)
        {
            int count = itemLogic.GetChildCount(primaryKey);
            if (count == 0)
            {
                //删除字典。
                int row = itemLogic.Delete(primaryKey);
                //删除字典选项。
                itemsDetailLogic.Delete(primaryKey);
                return row > 0 ? Success() : Error();
            }
            return Warning(string.Format("操作失败，请先删除该项的{0}个子级字典。", count));
        }


        [Route("System/Item/Detail")]
        [HttpGet, LoginChecked]
        public ActionResult Detail()
        {
            return View();
        }



        [Route("System/Item/GetListTree")]
        [HttpPost, LoginChecked]
        public ActionResult GetListTree()
        {
            var listAllItems = itemLogic.GetList();
            List<ZTreeNode> result = new List<ZTreeNode>();
            foreach (var item in listAllItems)
            {
                ZTreeNode model = new ZTreeNode();
                model.id = item.Id;
                model.pId = item.ParentId;
                model.name = item.Name;
                model.open = true;
                result.Add(model);
            }
            return Content(result.ToJson());
        }


        [Route("System/Item/GetListSelectTree")]
        [HttpPost, LoginChecked]
        public ActionResult GetListSelectTree()
        {
            var data = itemLogic.GetList();
            var treeList = new List<TreeSelect>();
            foreach (var item in data)
            {
                TreeSelect model = new TreeSelect();
                model.id = item.Id;
                model.text = item.Name;
                model.parentId = item.ParentId;
                treeList.Add(model);
            }
            return Content(treeList.ToTreeSelectJson());
        }


    }
}

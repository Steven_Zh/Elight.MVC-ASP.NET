﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNetCore.Mvc;
using Elight.WebUI.Core.Controllers;
using Elight.WebUI.Core.Filters;
using Elight.Logic.Sys;
using Elight.Entity.Sys;
using Elight.Utility.ResponseModels;
using Elight.Utility.Extension;
using Elight.Utility.Core;

namespace Elight.WebUI.Core.Controllers
{
    [HiddenApi]
    public class ItemsDetailController : BaseController
    {
        private SysItemsDetailLogic itemDetaillogic;

        public ItemsDetailController()
        {
            itemDetaillogic = new SysItemsDetailLogic();
        }


        [Route("System/ItemsDetail/Index")]
        [HttpGet, AuthorizeChecked, LoginChecked]
        public ActionResult Index()
        {
            return View();
        }
        [Route("System/ItemsDetail/Index")]
        [HttpPost, AuthorizeChecked, LoginChecked]
        public ActionResult Index(int pageIndex, int pageSize, string itemId, string keyWord)
        {
            int totalCount = 0;
            var pageData = itemDetaillogic.GetList(pageIndex, pageSize, itemId, keyWord, ref totalCount);
            var result = new LayPadding<SysItemDetail>()
            {
                result = true,
                msg = "success",
                list = pageData,
                count = totalCount//pageData.Count
            };
            return Content(result.ToJson());
        }

        [Route("System/ItemsDetail/Form")]
        [HttpGet, AuthorizeChecked, LoginChecked]
        public ActionResult Form()
        {
            return View();
        }


        [Route("System/ItemsDetail/Form")]
        [HttpPost, AuthorizeChecked,  LoginChecked]
        public ActionResult Form(SysItemDetail model)
        {
            if (model.Id.IsNullOrEmpty())
            {
                int row = itemDetaillogic.Insert(model);
                return row > 0 ? Success() : Error();
            }
            else
            {
                int row = itemDetaillogic.Update(model);
                return row > 0 ? Success() : Error();
            }
        }

        [Route("System/ItemsDetail/Detail")]
        [HttpGet, AuthorizeChecked, LoginChecked]
        public ActionResult Detail()
        {
            return View();
        }
        [Route("System/ItemsDetail/Delete")]
        [HttpPost, AuthorizeChecked, LoginChecked]
        public ActionResult Delete(string primaryKey)
        {
            int row = itemDetaillogic.Delete(primaryKey);
            return row > 0 ? Success() : Error();
        }


        [Route("System/ItemsDetail/GetForm")]
        [HttpPost, LoginChecked]
        public ActionResult GetForm(string primaryKey)
        {
            SysItemDetail entity = itemDetaillogic.Get(primaryKey);
            entity.IsDefault = entity.IsDefault == "1" ? "true" : "false";
            entity.IsEnabled = entity.IsEnabled == "1" ? "true" : "false";
            return Content(entity.ToJson());
        }
    }
}

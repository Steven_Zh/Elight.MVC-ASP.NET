﻿using log4net;
using log4net.Config;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Reflection;
using System.Text;

namespace Elight.Utility.Logs
{
    /// <summary>     
    /// NLog日志框架辅助类。     
    /// </summary>     
    public static class LogHelper
    {
        public static readonly ILog loginfo = LogManager.GetLogger("loginfo");
        public static readonly ILog logerror = LogManager.GetLogger("logerror");
        public static readonly ILog logdebug = LogManager.GetLogger("logdebug");
        public static readonly ILog logwarn = LogManager.GetLogger("logwarn");


        public static void Init(string configName)
        {
            XmlConfigurator.Configure(new FileInfo(configName));
        }

        public static void Info(string message)
        {
            if (loginfo.IsInfoEnabled)
            {
                loginfo.Info(message);
            }
        }

        public static void Info(string message, Exception ex)
        {
            if (loginfo.IsInfoEnabled)
            {
                loginfo.Info(message, ex);
            }
        }

        public static void Debug(string message)
        {
            if (logdebug.IsDebugEnabled)
            {
                logdebug.Debug(message);
            }
        }
        public static void Debug(string message, Exception ex)
        {
            if (logdebug.IsDebugEnabled)
            {
                logdebug.Debug(message, ex);
            }
        }

        public static void Warn(string message)
        {
            if (logwarn.IsWarnEnabled)
            {
                logwarn.Warn(message);
            }
        }
        public static void Warn(string message, Exception ex)
        {
            if (logwarn.IsWarnEnabled)
            {
                logwarn.Warn(message, ex);
            }
        }


        public static void Error(string message)
        {
            if (logerror.IsErrorEnabled)
            {
                logerror.Error(message);
            }
        }


        public static void Error(string message, Exception ex)
        {
            if (logerror.IsErrorEnabled)
            {
                logerror.Error(message, ex);
            }
        }
    }
    public enum Level
    {
        [Description("普通输出")]
        Trace,
        [Description("一般调试")]
        Debug,
        [Description("普通消息")]
        Info,
        [Description("警告信息")]
        Warn,
        [Description("一般错误")]
        Error,
        [Description("致命错误")]
        Fatal
    }
}


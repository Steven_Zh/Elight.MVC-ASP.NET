# elight.mvc
一款基于Web的通用管理系统轻量级解决方案。
## 快速开发
* 开发环境：VS2019
* 实验数据库：MySQL5.7
## 系统说明
* Elight.MVC是一套基于 ASP.NET MVC5 + Layui开发的通用管理系统快速开发框架。
* 支持SQL Server、MySQL、PostgreSQL、SQLite和Oracle等多种数据库类型。
* 该解决方案适用于OA、电商平台、CRM、物流管理、教务管理等各类管理系统开发。
* 兼容除IE8以下所有浏览器，暂不支持移动端。
* 初始用户名：admin 密码：123456
## 变更记录
* 将EF框架改成SqlSugar
* 数据库：MySQL5.6及以上版本
* 升级了LayUI版本 目前版本:2.2.6
* 修改了原来的一些bug
* 简化了代码逻辑，更容易理解
* 将.netcore 与 .NET Framework合在一起，方便维护
* 新增UEditor使用DEMO   访问 http://ip:port/ueditor.html
* 默认使用SQLite 如果要使用其他数据库 
* .NETFramework 请到Elight.WebUI/Web.config 中修改类型以及连接字符串
* .NetCore 请到Elight.WebUI.Core/appsettings.json 中修改类型以及连接字符串
* 数据库DDL和DML 在App_Data下 .NETCore在wwwroot/Data下
* 原始ASP.NET代码github：https://github.com/esofar/elight.mvc


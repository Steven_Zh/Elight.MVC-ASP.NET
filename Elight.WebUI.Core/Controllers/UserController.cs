﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Elight.WebUI.Core.Filters;
using Elight.Logic.Sys;
using Elight.Entity.Sys;
using Elight.Utility.ResponseModels;
using Elight.Utility.Core;

namespace Elight.WebUI.Core.Controllers
{
    //[HiddenApi]
    public class UserController : BaseController
    {
        private SysUserLogic userLogic;
        private SysUserRoleRelationLogic userRoleRelationLogic;
        private SysUserLogOnLogic userLogOnLogic;
        public UserController()
        {
            userLogic = new SysUserLogic();
            userRoleRelationLogic = new SysUserRoleRelationLogic();
            userLogOnLogic = new SysUserLogOnLogic();
        }


        [Route("System/User/Index")]
        [HttpGet, AuthorizeChecked, LoginChecked]
        public ActionResult Index()
        {
            return View();
        }


        [Route("System/User/Index")]
        [HttpPost, AuthorizeChecked, LoginChecked]
        public ActionResult Index(int pageIndex, int pageSize, string keyWord)
        {
            int totalCount = 0;
            var pageData = userLogic.GetList(pageIndex, pageSize, keyWord, ref totalCount);
            var result = new LayPadding<SysUser>()
            {
                result = true,
                msg = "success",
                list = pageData,
                count = totalCount//pageData.Count
            };
            return Content(result.ToJson());
        }


        [Route("System/User/Form")]
        [HttpGet, AuthorizeChecked, LoginChecked]
        public ActionResult Form()
        {
            return View();
        }

        [Route("System/User/Form")]
        [HttpPost, AuthorizeChecked, LoginChecked]
        public ActionResult Form([FromForm] SysUser model, [FromForm] string password, [FromForm] string roleIds)
        {
            if (model.Id.IsNullOrEmpty())
            {
                DateTime defaultDt = DateTime.Today;
                DateTime.TryParse(model.StrBirthday + " 00:00:00", out defaultDt);
                model.Birthday = defaultDt;
                int row = userLogic.Insert(model, password, roleIds.SplitToList().ToArray());
                return row > 0 ? Success() : Error();
            }
            else
            {
                DateTime defaultDt = DateTime.Today;
                DateTime.TryParse(model.StrBirthday + " 00:00:00", out defaultDt);
                model.Birthday = defaultDt;
                //更新用户基本信息。
                int row = userLogic.UpdateAndSetRole(model, roleIds.SplitToList().ToArray());
                //更新用户角色信息。
                return row > 0 ? Success() : Error();
            }
        }


        [Route("System/User/Detail")]
        [HttpGet, AuthorizeChecked, LoginChecked]
        public ActionResult Detail()
        {
            return View();
        }


        [Route("System/User/GetForm")]
        [HttpPost, LoginChecked]
        public ActionResult GetForm(string primaryKey)
        {
            SysUser entity = userLogic.Get(primaryKey);
            entity.StrBirthday = entity.Birthday.Value.ToString("yyyy-MM-dd");
            entity.RoleId = userRoleRelationLogic.GetList(entity.Id).Select(c => c.RoleId).ToList();

            return Content(entity.ToJson());
        }


        [Route("System/User/Delete")]
        [HttpPost, AuthorizeChecked, LoginChecked]
        public ActionResult Delete(string userIds)
        {
            //多用户删除。
            int row = userLogic.Delete(userIds.SplitToList().ToArray());
            userRoleRelationLogic.Delete(userIds.SplitToList().ToArray());
            userLogOnLogic.Delete(userIds.SplitToList().ToArray());
            return row > 0 ? Success() : Error();
        }


        [Route("System/User/CheckAccount")]
        [HttpPost, LoginChecked]
        public ActionResult CheckAccount(string userName)
        {
            var userEntity = userLogic.GetByUserName(userName);
            if (userEntity != null)
            {
                return Error("已存在当前用户名，请重新输入");
            }
            return Success("恭喜您，该用户名可以注册");
        }


    }
}
